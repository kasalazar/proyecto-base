$(function(){
    $('.carousel').carousel({
        interval: 5000
    });         
});

$(function(){
    $("[data-toggle='tooltip']").tooltip()
});

$(function(){
    $("[data-toggle='popover']").popover()
});

$(function(){
    $('#modalContacto').on('show.bs.modal',function(e){
        console.log('El modal se está mostrando');
    });
    $('#contactoBtn').removeClass('btn-contacto');
    $('#contactoBtn').addClass('btn-primary');


    $('#modalContacto').on('shown.bs.modal',function(e){
        console.log('El modal se mostró');
        $('#contactoBtn').prop('disabled',true);
    });

    $('#modalContacto').on('hide.bs.modal',function(e){
        console.log('El modal se oculta');
    });

    $('#modalContacto').on('hidden.bs.modal',function(e){
        console.log('El modal se ocultó');
        $('#contactoBtn').prop('disabled',false);
    });
});